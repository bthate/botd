# This file is placed in the Public Domain.

__version__ = 1

def ver(event):
    event.reply("BOTL %s - pure python3 bot library" % __version__)
