# This file is placed in the Public Domain.

__version__ = 27

def ver(event):
    event.reply("BOTD %s - 24/7 channel daemon, survives reboots !!!" % __version__)
