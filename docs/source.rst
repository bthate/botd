.. _source:

SOURCE
======

BOTL provides the following modules:

.. autosummary::
    :toctree: 
    :template: module.rst


    botl                 - pure python3 bot library
    botl.bus             - list of bots
    botl.clk             - clock/repeater
    botl.cmd.adm         - admin
    botl.cmd.cfg         - configuration
    botl.cmd.cmd         - list of commands
    botl.cmd.fnd         - find
    botl.csl             - console
    botl.dbs             - databases
    botl.evt             - events
    botl.hdl             - handler
    botl.irc             - internet relay chat
    botl.itr             - introspection
    botl.prs             - parser
    botl.tbl             - tables
    botl.thr             - threads
    botl.usr             - users
    botl.utl             - utilities
    botl.ver             - version

BOTD has the following modules:

.. autosummary::
    :toctree: 
    :template: module.rst

    botd.log            - log items
    botd.rss            - rich site syndicate
    botd.tdo            - todo items
    botd.udp            - UDP to IRC relay
